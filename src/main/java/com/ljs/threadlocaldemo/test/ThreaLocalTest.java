package com.ljs.threadlocaldemo.test;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ThreaLocalTest {
   //用线程池来模拟高并发的情况
    public static ExecutorService executorService= Executors.newFixedThreadPool(16);
    //用threadLocal,初始化，threadlocal实际上是操作threadLocalMap，来维护当前的线程，达到线程安全的情况
    private static ThreadLocal<SimpleDateFormat> dateFormatThreadLocal = ThreadLocal.withInitial(() -> new SimpleDateFormat("mm:ss"));

    public static String getDate(int seconds){
        Date date=new Date(1000* seconds);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("mm:ss");
      return   simpleDateFormat.format(date);

    }

    public static void main(String[] args) {
        //这么多对象的创建是有开销的，并且在使用完之后的销毁同样是有开销的，同时存在在内存中也是一种内存的浪费。
        hasLocal();
       // noLocal();
    }

    /**
     * 没有用threadLocal的方法
     */
    public static void noLocal(){
        for(int i=0;i<1000;i++){
            int finalI = i;
            executorService.submit(() ->{
                System.out.println("没有用threadLocal当前线程"+Thread.currentThread().getName()+"获取的时间"+getDate(finalI));  ;
            });
        }
        executorService.shutdown();
    }
    public static void hasLocal(){
        for(int i=0;i<1000;i++){
            int finalI = i;
            executorService.submit(() ->{
                System.out.println("用threadLocal当前线程"+Thread.currentThread().getName()+"获取的时间"+date(finalI));  ;
            });
        }
        executorService.shutdown();
        //整个流程执行完毕要删除,不然会占用
        dateFormatThreadLocal.remove();
    }
    private static String date(int seconds){
        Date date = new Date(1000 * seconds);
        //从threadLocal中拿对象
        SimpleDateFormat dateFormat = dateFormatThreadLocal.get();
        return dateFormat.format(date);
    }
}
